import {useState} from "react";
import {nanoid} from "nanoid";
import AddTaskForm from "./AddTaskForm";
import Task from "./Task";

const App = () => {
    const [addNewTask, setAddNewTask] = useState(
        [
            {newTaskName: 'Add New Task', done: false, id: nanoid()}
        ]
    );

    const [addTask, setAddTask] = useState(
        [
            {taskName: 'Buy milk', done: false, id: nanoid()},
            {taskName: 'Walk with god', done: false, id: nanoid()},
            {taskName: 'Do homework', done: false, id: nanoid()},
        ]
    );

    const currentTask = (id, taskName) => {
        setAddNewTask(addNewTask.map(task => {
            if (task.id === id) {
                return {...task, newTaskName: taskName}
            }
            return task;
        }))
    };

    const add = (taskText) => {
        setAddTask([...addTask, {
            taskName: taskText, done: false, id: nanoid()
        }]);
    };

    console.log(addTask);

    const checkBoxClick = (id, target) => {
        setAddTask(
            addTask.map(task => {
                if (task.id === id) {
                    target.className = 'strikethrough-line';
                    if (task.done === true) {
                        target.classList.remove('strikethrough-line')
                    }
                    return {...task, done: !task.done};
                }
                return task;
            })
        )
    };

    const removeTask = id => {
        setAddTask(
            addTask.filter(task => task.id !== id)
        )
    };

    return (
        <>
            {addNewTask.map(task => {
                return <AddTaskForm
                    key={task.id}
                    taskText={task.newTaskName}
                    onInputChange={e => currentTask(task.id, e.target.value)}
                    addTask={() => add(task.newTaskName)}
                />
            })}

            {addTask.map(tasks => {
                return <Task
                    key={tasks.id}
                    taskName={tasks.taskName}
                    onRemove={() => removeTask(tasks.id)}
                    onCheckboxClick={(e) => checkBoxClick(tasks.id, e.currentTarget)}
                />
            })}
        </>
    )
};

export default App;
