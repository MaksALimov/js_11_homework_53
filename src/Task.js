import React from 'react';
import './Task.css'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faTrashAlt} from "@fortawesome/free-regular-svg-icons";

const Task = props => {
    return (
        <div className="wrapper">
            <label>
                <input onClick={props.onCheckboxClick} type="checkbox" name="task"/>
                <span>{props.taskName}</span>
            </label>
            <button className="trash" onClick={props.onRemove}><FontAwesomeIcon icon={faTrashAlt}/></button>
        </div>
    );
};

export default Task;