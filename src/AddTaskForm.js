import React from 'react';

const AddTaskForm = props => {
    return (
        <div className="newTask">
            <input
                type="text"
                value={props.taskText}
                onChange={props.onInputChange}
            />
            <button className="add" onClick={props.addTask}>Add</button>
        </div>
    );
};

export default AddTaskForm;